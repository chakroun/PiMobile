/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.models;
import java.util.Date;
/**
 *
 * @author DIARRA
 */
public class Reclamation {
     private int id;
     private String sujet;
     private String nature;
     private String date;
     private String description;
     private String nom;
     private String etat;
     private int level;
     private String email;

    public Reclamation() {
    }

    public Reclamation(int id, String sujet, String nature, String date, String description, String nom, String etat, int level, String email) {
        this.id = id;
        this.sujet = sujet;
        this.nature = nature;
        this.date = date;
        this.description = description;
        this.nom = nom;
        this.etat = etat;
        this.level = level;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Reclamation{" + "id=" + id + ", sujet=" + sujet + ", nature=" + nature + ", date=" + date + ", description=" + description + ", nom=" + nom + ", etat=" + etat + ", Telephone=" + level + ", email=" + email + '}';
    }
      public String info() {
        return "Reclamation{+ sujet=" + sujet + ", description=" + description +'}';
    }
       public String infoId() {
        return ""+id;
    }
       public String AfficherDetails() {
        return "La Description :"+description+"\n"+"le Sujet :"+sujet+"\n"+"La nature: "+nature+"\n";
    }
      
//     @Override
//    public int hashCode() {
//        int hash = 5;
//        hash = 67 * hash + Objects.hashCode(this.id);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Reclamation other = (Reclamation) obj;
//        if (!Objects.equals(this.id, other.id)) {
//            return false;
//        }
//        return true;
//    }
}
