/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.models;
/**
 *
 * @author wister
 */
public class User {

    /**
     *
     */
public static int connectedUser;
private int id;
private String username;
private String image;
private String nom;
private String email;
private String password;
private int point;
private int pointTotal;
private int carte;
private String roles;
private String position;
private static int id_reservation;


    public User() {
    }

    public User(int id) {
        this.id = id;
    }

    public User(int id, String username, String nom) {
        this.id = id;
        this.username = username;
        this.nom = nom;
        
    }

    public User(int id, String username, String email, int point) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.point = point;
    }

  public User(int id, String username, String email,String password,String image, String nom,String position) {
        this.id = id;
        this.username = username;
        this.image=image;
        this.nom = nom;
        this.email = email;
        this.password = password;
        this.position = position;
        
    }
   
     public User(int id, String username, String email,String password,String image, String nom,String position,String roles) {
        this.id = id;
        this.username = username;
        this.image=image;
        this.nom = nom;
        this.email = email;
        this.password = password;
        this.position = position;
        this.roles =roles;
    }

    public User(int id, String username, String email,String password,String roles,String image,int point, String nom,String position) {
        this.id = id;
        this.username = username;
        this.image = image;
        this.nom = nom;
        this.email = email;
        this.password = password;
        this.point = point;
        this.roles = roles;
        this.position = position;
    }
     
    

    public String getUsername() {
        return username;
    }

    public User(int id, String username) {
        this.id = id;
        this.username = username;
    }
    public User(int id, String username,int point) {
        this.id = id;
        this.username = username;
        this.point = point;
    }
    

    public void setUsername(String username) {
        this.username = username;
    }
    

    public User(int id, String nom, String email, String password) {
        this.id = id;
        this.nom = nom;
        this.email = email;
        this.password = password;
    }
    
     public User(String nom, String email, String password) {
        this.nom = nom;
        this.email = email;
        this.password = password;
    }

    public User(String username, String nom, String email, String password) {
        this.username = username;
        this.nom = nom;
        this.email = email;
        this.password = password;
    }
    

    public int getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getNom() {
        return nom;
    }

    public int getPoint() {
        return point;
    }

    public int getPointTotal() {
        return pointTotal;
    }

    public int getCarte() {
        return carte;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void setPointTotal(int pointTotal) {
        this.pointTotal = pointTotal;
    }

    public void setCarte(int carte) {
        this.carte = carte;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public static int getConnectedUser() {
        return connectedUser;
    }

    public static void setConnectedUser(int connectedUser) {
        User.connectedUser = connectedUser;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public static int getId_reservation() {
        return id_reservation;
    }

    public static void setId_reservation(int id_reservation) {
        User.id_reservation = id_reservation;
    }
    


    @Override
    public String toString() {
        return "User{" + "id=" + id + ", image=" + image + ", nom=" + nom + ", email=" + email + ", password=" + password + ", point=" + point + ", pointTotal=" + pointTotal + ", carte=" + carte + '}';
    }

   
          
    
}
