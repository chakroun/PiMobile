/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.models;

/**
 *
 * @author Daddati
 */
public class Favoris {
    private String id;
    private String libelle;
    private String categorie;
    private String description;
    private String imageService;
    private String prix;
    private String nbr;

    public Favoris() {
    }

    public Favoris(String libelle, String categorie, String description) {
        this.libelle = libelle;
        this.categorie = categorie;
        this.description = description;
    }

    public Favoris(String libelle, String categorie, String description, String imageService, String prix, String nbr) {
        this.libelle = libelle;
        this.categorie = categorie;
        this.description = description;
        this.imageService = imageService;
        this.prix = prix;
        this.nbr = nbr;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageService() {
        return imageService;
    }

    public void setImageService(String imageService) {
        this.imageService = imageService;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getNbr() {
        return nbr;
    }

    public void setNbr(String nbr) {
        this.nbr = nbr;
    }

    
   
}
