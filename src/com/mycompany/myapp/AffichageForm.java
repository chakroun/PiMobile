/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.models.Planing;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author wiem
 */
public class AffichageForm {
  
     private Resources theme;
      SimpleDateFormat inputFormat; 
        ArrayList<Planing> lise = new ArrayList<>();
        ArrayList<Planing> Plan = new ArrayList<>();
        ArrayList<User> listUser = new ArrayList<>();
        ArrayList<User> liseUser = new ArrayList<>();
    Form list,f;
    EncodedImage encoded;
    public AffichageForm() {
        UIBuilder ui = new UIBuilder();
        displayList();    
    }
    public void displayList() {
        list = new Form("List de mes evenements", BoxLayout.y());
        Container c = cellForRow();
        list.addComponent(c);
        list.show();
        }
        public Container cellForRow() {
                Container ctn1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
                Container ctn2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
                Container ctn3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
               
                //System.out.println(createurAffiche);
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/SelectAllPlan.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                inputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        encoded = EncodedImage.create("/event.jpg");
                    } catch (IOException ex) {
                    }
                lise = getListPlaning(new String(con.getResponseData()));
                  LoginForm lg = new LoginForm();
                String createurAffiche = lg.getU().getNom();
                for (Planing p1 : lise) {
                  System.out.println(p1.getCreateur());
                    if (p1.getCreateur().equals(createurAffiche))
                    {
                         Label nEvent = new Label();
                         nEvent = new Label("Nom de l'evenement: "+"\n"+ p1.getNom_evenement()+"\n");
                         Label DEvent = new Label();
                         DEvent = new Label("Date: " +p1.getHoraire_planing()+"\n");
                         Label Etat = new Label();
                         Etat = new Label("Etat: " + p1.getEtat_event());   
              ctn2.add(encoded);
              ctn3.add(nEvent);
              ctn3.add(DEvent);
              ctn3.add(Etat);
                    }
                    else 
                    {
                        System.out.println("pas d'evenement");
                    }
              }
              ctn1.add(ctn2);
              ctn1.add(ctn3);
            }
      });
          NetworkManager.getInstance().addToQueue(con);
          return ctn1;
    }
     public ArrayList<Planing> getListPlaning(String json) {
          ArrayList<Planing> listPlaning = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> planing = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) planing.get("planing");
            for (Map<String, Object> obj : list) {
               Planing p = new Planing();
                //p.setId_service((Service)obj.get("id_service"));
                p.setNom_evenement(obj.get("nom_evenement").toString());
                String inputDate=obj.get("horaire_planing").toString();
                Date inputDate2 = inputFormat.parse(inputDate);
                p.setHoraire_planing(inputDate2);
                p.setEtat_event(obj.get("etat_event").toString());
                p.setCreateur(obj.get("createur").toString());
                listPlaning.add(p);
                System.out.println(inputDate);
              }
        } catch (IOException ex) {
        } catch (ParseException ex) {
             //ogger.getLogger(AffichageForm.class.getName()).log(Level.SEVERE, null, ex);
         }
        return listPlaning;
    }
     
    public ArrayList<User> getListUser(String json) {
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> User = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) User.get("fos_user");
            for (Map<String, Object> obj : list) {
                User u = new User();
                u.setId(Integer.parseInt(obj.get("id").toString()));
                u.setNom(obj.get("nom").toString());
                listUser.add(u);
            }
        } catch (IOException ex) {
        }
        System.out.println(listUser);
        return listUser;
    }
       public Form getF() {
        return f;
    }  
}
