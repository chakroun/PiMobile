/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.db.Cursor;
import com.codename1.db.Database;
import com.codename1.db.Row;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import java.io.IOException;
import java.util.ArrayList;
import com.codename1.ui.util.Resources;
import com.mycompany.models.Service;


/**
 *
 * @author Daddati
 */
public class FavorisForm extends Form{
        EncodedImage encoded;

     private Form current, list, add,f;
    private Resources theme1;
    Database db;

    boolean exist;
    //ArrayList<Favoris> favorises = new ArrayList<>();
    ArrayList<Service> favorises = new ArrayList<>();

public void SqlLite(Resources theme1,Service s) {
    
       exist = Database.exists("favoris");

        try {
            db = Database.openOrCreate("favoris");

            if (!exist) {
                db.execute("create table service(id INTEGER PRIMARY KEY, nom text, categorie text, description text, image text, prix text, nbr text);");
            }

        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
      
        
        selectFavoris(s);

    }
    public void SqlLite1(Resources theme1,Service s) {

       exist = Database.exists("favoris");

        try {
            db = Database.openOrCreate("favoris");

            if (!exist) {
                db.execute("create table service(id INTEGER PRIMARY KEY, nom text, categorie text, description text, image text, prix text, nbr text);");
            }

        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        addFavoris(s);

    }

    public void insertFavoris(Service s) {
        try {
            db.execute("insert into service (categorie, nom, description) values('" + s.getCategorie()+ "','" + s.getLibelle()+ "','" + s.getDescription()+ "')");
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        favorises.clear();
        selectFavoris(s);
    }

       public void updateFavoris(Service s) {
        try {
            db.execute("update service set nom='"+s.getLibelle()+"', categorie='"+s.getCategorie()+"', description='"+s.getDescription()+"' where id='"+s.getIdService()+"'");
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        favorises.clear();
        selectFavoris(s);
    }

    public void deleteFavoris(Service s) {
        try {
            db.execute("delete from service where id = '"+s.getIdService()+"'");
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        favorises.clear();
        selectFavoris(s);
    }

    public void selectFavoris(Service s) {
     
        try {
            Cursor c = db.executeQuery("select * from service");

            while (c.next()) {
                Row r = c.getRow();

                 s = new Service();

                s.setIdService(Integer.parseInt(r.getString(0)));
                s.setLibelle(r.getString(1));
                s.setCategorie(r.getString(2));
                s.setDescription(r.getString(3));

                

                favorises.add(s);

            }
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }

        displayFavoris(theme1,s);
    }

    public void displayFavoris(Resources theme1,Service a) {
        list = new Form("Favoris", BoxLayout.y());
                

//list.getToolbar().addCommandToSideMenu("Sortie en Famille", null, new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//           SortirEnFamilleForm about = new SortirEnFamilleForm(theme1);
//           about.getF().show();
//            }
//        });
        
        for ( Service s : favorises) {

            Container ctn1 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

            Label lblName = new Label(" " + s.getLibelle()+ " ");
            Label lblCategorie = new Label("  " + s.getCategorie());
            Label lblDescription = new Label("  "+s.getDescription());
           
         
               
            lblName.addPointerReleasedListener(e->{
            
                editFavoris(s);
                
            });
            
            ctn1.setLeadComponent(lblName);

            ctn1.add(lblName);
            ctn1.add(lblCategorie);
            ctn1.add(lblDescription);
//             ctn1.add(imgProfile);

            list.add(ctn1);


        }
        Command cmdBack = new Command("Back");

        list.getToolbar().addCommandToLeftBar(cmdBack);

        list.addCommandListener(e -> {
            addFavoris(a);

        });
        list.show();
    }
    
    public void addFavoris(Service s){
        f = new Form("Ajouter un service", BoxLayout.y());
         
                 
        Label lblNom = new Label("Nom");
        TextField tfNom = new TextField("", "libelle");
        Label lblCategorie = new Label("Categorie");
        TextField tfCategorie = new TextField("", "categorie");
        Label lblDescription = new Label("Description");
        TextField tfDescription = new TextField("", "description");
//        Command cmdBack = new Command("Back");
//
//        f.getToolbar().addCommandToLeftBar(cmdBack);
//
//        f.addCommandListener(e -> {
//          
//     new SortirEnFamilleForm(theme1).show();
//       
//
//        });
                  
     
        Button btnOk = new Button("Ajouter aux Favoris");
        
        tfNom.setText(String.valueOf(s.getLibelle()));
        tfCategorie.setText(s.getCategorie());
        tfDescription.setText(s.getDescription());
       


        f.add(tfNom);
        f.add(tfCategorie);
       f.add(tfDescription);
       
        f.add(btnOk);

        
         btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               
insertFavoris(new Service(tfNom.getText(), tfCategorie.getText(), tfDescription.getText())); 
                        
                    }
                });

     f.show();
    }

    
    
    public void editFavoris(Service s)
    {

      f = new Form("Modifier favoris", BoxLayout.y());
     
        
       
                 
        Label lblNom = new Label("Nom");
        TextField tfNom = new TextField("", "libelle");
        Label lblCategorie = new Label("Categorie");
        TextField tfCategorie = new TextField("", "categorie");
        Label lblDescription = new Label("Description");
        TextField tfDescription = new TextField("", "description");
       
                  
     
        Button btnOk = new Button("Modifier");
        
        tfNom.setText(String.valueOf(s.getLibelle()));
        tfCategorie.setText(s.getCategorie());
        tfDescription.setText(s.getDescription());
       


        f.add(tfNom);
        f.add(tfCategorie);
       f.add(tfDescription);
       
        f.add(btnOk);

        
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
               
               // updateFavoris(Favoris(new Favoris(tfNom.getText(), tfCategorie.getText(), tfDescription.getText())); 
                  updateFavoris(s);
                    }
                });
//        Command cmdBack = new Command("Back");
//        
//        f.getToolbar().addCommandToLeftBar(cmdBack);
//        
//        f.addCommandListener(e->{
//        
//            displayFavoris(theme1,s);
//        
//        });
              
        Command cmdDelete = new Command("Supprimer");
        
        f.getToolbar().addCommandToRightBar(cmdDelete);
        
        f.addCommandListener(e->{
        
            deleteFavoris(s);
        
        });
               f.show();
       
    }
}