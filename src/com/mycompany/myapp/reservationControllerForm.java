/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;

import com.codename1.ui.util.UIBuilder;
import com.mycompany.models.Promotion;
import com.mycompany.models.Reservation;
import com.mycompany.models.Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hanen
 */
public class reservationControllerForm extends Form {

    public Form current, listt, menu, f1,f;
    public Resources theme, theme1;
    EncodedImage encoded;
    public int id_s,u;
    public double p;
    int promo;
    public int a;
    public int nbplaces, nbplacesD;
    public int restant;
    public Container ctn3, ctn2;
    String image;
    ArrayList<Promotion> listpromo = new ArrayList<>();

    ArrayList<Reservation> listres = new ArrayList<>();
    ArrayList<Service> lists = new ArrayList<>();
Form f3 = new Form();
    SpanLabel sp = new SpanLabel();

    public void afficherrestaurant(Resources theme) {
         try {
            encoded = EncodedImage.create("/load.png");
            
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
UIBuilder uib = new UIBuilder();
//Form f3= new Form();


        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectserviceRestaurant.php");
         Command cmdBack = new Command("Back");

        f3.getToolbar().addCommandToLeftBar(cmdBack);

        f3.addCommandListener(e -> {
          
      SortirEnFamilleForm R = new SortirEnFamilleForm(theme);
      R.getF().show();
       

           // new SortirEnFamilleForm(theme).s
        });
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
             public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListser(new String(con.getResponseData())));
                ArrayList<Service> l = getListser(new String(con.getResponseData()));
                for (Service e : l) {
                    SpanLabel sp = new SpanLabel();
//                  f1.add(sp);
                    sp.setText(e.toString());
                   // System.out.println(l);
                    Button b = new Button();
//                    Container ctn = new Container();
//                            {       ctn.add(cellForRow(e,theme));}

                    // f1.add(b);
                    b.setText("reserver");
                    //f3.add(sp);
                                f3.add(cellForRow(e,theme));

                    
                            
                }
               
            }
        });

        NetworkManager.getInstance().addToQueue(con);
f3.show();
    };

    public void affichercinema(Resources theme) {
         try {
            encoded = EncodedImage.create("/load.png");
            
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
UIBuilder uib = new UIBuilder();
//Form f3= new Form();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectservicecinema.php");
         Command cmdBack = new Command("Back");

        f3.getToolbar().addCommandToLeftBar(cmdBack);

        f3.addCommandListener(e -> {
          
      SortirEnFamilleForm R = new SortirEnFamilleForm(theme);
      R.getF().show();
       

           // new SortirEnFamilleForm(theme).s
        });
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
             public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListser(new String(con.getResponseData())));
                ArrayList<Service> l = getListser(new String(con.getResponseData()));
                for (Service e : l) {
                    SpanLabel sp = new SpanLabel();
//                  f1.add(sp);
                    sp.setText(e.toString());
                  //  System.out.println(l);
                    Button b = new Button();
//                    Container ctn = new Container();
//                            {       ctn.add(cellForRow(e,theme));}

                    // f1.add(b);
                    b.setText("reserver");
                    //f3.add(sp);
                                f3.add(cellForRow(e,theme));

                    
                            
                }
               
            }
        });

        NetworkManager.getInstance().addToQueue(con);
f3.show();
    };
     public void affichercirque(Resources theme) {
          try {
            encoded = EncodedImage.create("/load.png");
            
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
UIBuilder uib = new UIBuilder();
//Form f3= new Form();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectservicecirque.php");
         Command cmdBack = new Command("Back");

        f3.getToolbar().addCommandToLeftBar(cmdBack);

        f3.addCommandListener(e -> {
          
      SortirEnFamilleForm R = new SortirEnFamilleForm(theme);
      R.getF().show();
       

           // new SortirEnFamilleForm(theme).s
        });
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
             public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListser(new String(con.getResponseData())));
                ArrayList<Service> l = getListser(new String(con.getResponseData()));
                for (Service e : l) {
                    SpanLabel sp = new SpanLabel();
//                  f1.add(sp);
                    sp.setText(e.toString());
                   // System.out.println(l);
                    Button b = new Button();
//                    Container ctn = new Container();
//                            {       ctn.add(cellForRow(e,theme));}

                    // f1.add(b);
                    b.setText("reserver");
                    //f3.add(sp);
                                f3.add(cellForRow(e,theme));

                    
                            
                }
               
            }
        });

        NetworkManager.getInstance().addToQueue(con);
f3.show();
    };
    public void afficheparc(Resources theme) {
         try {
            encoded = EncodedImage.create("/load.png");
            
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
UIBuilder uib = new UIBuilder();
//Form f3= new Form();
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectserviceparc.php");
         Command cmdBack = new Command("Back");

        f3.getToolbar().addCommandToLeftBar(cmdBack);

        f3.addCommandListener(e -> {
          
      SortirEnFamilleForm R = new SortirEnFamilleForm(theme);
      R.getF().show();
       

           // new SortirEnFamilleForm(theme).s
        });
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
             public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListser(new String(con.getResponseData())));
                ArrayList<Service> l = getListser(new String(con.getResponseData()));
                for (Service e : l) {
                    SpanLabel sp = new SpanLabel();
//                  f1.add(sp);
                    sp.setText(e.toString());
                   // System.out.println(l);
                    Button b = new Button();
//                    Container ctn = new Container();
//                            {       ctn.add(cellForRow(e,theme));}

                    // f1.add(b);
                    b.setText("reserver");
                    //f3.add(sp);
                                f3.add(cellForRow(e,theme));

                    
                            
                }
               
            }
        });

        NetworkManager.getInstance().addToQueue(con);
f3.show();
    };

    public ArrayList<Service> getListser(String json) {
        JSONParser j = new JSONParser();
        Map<String, Object> services;
        try {
            services = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) services.get("service");


            for (Map<String, Object> obj : list) {
                Service s = new Service();
                s.setCategorie(obj.get("categorie").toString());
                s.setDescription(obj.get("description").toString());
                s.setPrix(Integer.parseInt(obj.get("prix").toString()));
                s.setLibelle(obj.get("libelle").toString());
                s.setIdService(Integer.parseInt(obj.get("id_service").toString()));
                s.setNbPlacesDispo(Integer.parseInt(obj.get("nb_places_dispo").toString()));
                s.setImageService(obj.get("image_service").toString());
                lists.add(s);
                
                
            }

        } catch (IOException ex) {
        }
        return lists;

    }

    public Container cellForRow(Service s, Resources theme ) {
    
        
        
        Container ctn3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Container ctn1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container ctn2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Label lblSurnom = new Label( "prix : " + s.getPrix());

        ctn1.setLeadComponent(lblSurnom);

        ctn2.add(new Label("Nom: "+s.getLibelle()));
        ctn2.add(new Label("Detail: "+s.getDescription()));
        ctn2.add(new Label("Places disponibles: "+s.getNbPlacesDispo()));
        ctn2.add(lblSurnom);
        Image imgServer = URLImage.createToStorage(encoded, s.getLibelle()+s.getPrix(), "http://localhost/pi/images/"+s.getImageService());
        
        
        ImageViewer imgProfile = new ImageViewer(imgServer);
        imgProfile.setCellRenderer(focusScrolling);
        ctn1.add(imgProfile);
        
        lblSurnom.addPointerReleasedListener(e -> {

            //display detail
            displayres(s, theme);
            id_s = s.getIdService();
            p = s.getPrix();
            nbplacesD = s.getNbPlacesDispo();
         image= s.getImageService();
             u=LoginForm.getU().getId();
                    

        });
        ctn1.setLeadComponent(lblSurnom);

        ctn1.add(ctn2);
        ctn3.add(ctn1);
Button bntF = new Button("Favoris");
        ctn3.add(bntF);
        bntF.addActionListener(e -> {

            displayDetail(s);
        
        });
        return ctn3;
    }

    public void afficherpromotion() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectpromotion.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListEtudiant(new String(con.getResponseData())));
                ArrayList<Promotion> l = getListpromo(new String(con.getResponseData()));
                for (Promotion p : l) {
                  //  System.out.println(l);
                    a = p.getIdPromo();
                    System.out.println(a);
                    if (id_s == a) {
                        promo = p.getIdPromo();
                        System.out.println(promo);
                    }
                }
            }
        });
        NetworkManager.getInstance().addToQueue(con);

    }

    public ArrayList<Promotion> getListpromo(String json) {
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> service = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) service.get("promotion");
            for (Map<String, Object> obj : list) {
                Promotion p = new Promotion();
                p.setIdPromo(Integer.parseInt(obj.get("id_promo").toString()));
                p.setPrix_promo(Integer.parseInt(obj.get("prixpromo").toString()));
                p.setService((Service) (obj.get("id_service")));
                listpromo.add(p);
            }
        } catch (IOException ex) {
        }

        System.out.println(listpromo);
        return listpromo;
    }

    public void displayres(Service s, Resources theme) {
        Command cmd1 = new Command("accueil");
        UIBuilder uib = new UIBuilder();
        f = new Form("reservation", BoxLayout.y());
        TextField nbplace = new TextField();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Picker date = new Picker();
        Button Log = new Button("reserver");
        f.add(nbplace);
        f.add(date);
        f.add(Log);

        Log.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                nbplaces = Integer.parseInt(nbplace.getText());

                restant = nbplacesD - nbplaces;
              //  System.out.println(promo + "pppppppp");
              //  System.out.println(a + "ffff");

                ConnectionRequest req = new ConnectionRequest();
                req.setUrl("http://localhost/pi/insertR.php?nb_placesreserve=" + nbplaces + "&date_reservation=" + date.getText() + "&id=" + id_s + "&prix=" + p + "&placesrestant=" + restant+"&idu=" +u);

                s.setNbPlacesDispo(restant);
               // System.out.println("http://localhost/pi/insertR.php?nb_placesreserve=" + nbplace.getText() + "&date_reservation=" + date.getText() + "&id=" + id_s + "");

               // System.out.println(restant + "fdddddd");
                req.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {

                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                       // System.out.println(s);
                       
                            Dialog.show("Confirmation", "reservation effectué avec succes", "Ok", null);
                            Message m = new Message("nombre de places reserve"+nbplaces+"prix restant à payer "+p );
//                    m.getAttachments().put("ee", "text/plain");
//                    m.getAttachments().put("a", "image/png");
                    Display.getInstance().sendMessage(new String[]{LoginForm.getU().getEmail()}, "Reservation", m);

                            AccueilForm af = new AccueilForm(theme);
                       af.getF().show();
                       
                          //  Dialog.show("Erreur", "erreur", "Ok", null);
                                               
                    }
                });

                NetworkManager.getInstance().addToQueue(req);
               
            }
            
        });

       // f.show();

        Command cmdBack = new Command("Back");

        f.getToolbar().addCommandToLeftBar(cmdBack);

        f.addCommandListener(e -> {

            f3.showBack();

        });

       f.show();

    }

    public Form getF() {
        return f;
    }
      private void displayDetail(Service s) {
      
        FavorisForm cmd1 = new  FavorisForm();
        cmd1.SqlLite1(theme,s);
  }

    public void stop() {
        current = Display.getInstance().getCurrent();
        if (current instanceof Dialog) {
            ((Dialog) current).dispose();
            current = Display.getInstance().getCurrent();
        }
    }

}
