/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;

import com.codename1.ui.Form;

import com.codename1.ui.util.Resources;
import java.util.List;

import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;

import com.codename1.ui.util.UIBuilder;

import java.io.IOException;
import java.util.ArrayList;

import java.util.Map;

/**
 *
 * @author wister
 */
class NosFamillesForm {

    Form f;
    ArrayList<Integer> listIds;
    EncodedImage encoded;

    public NosFamillesForm(Resources theme) {
        
        
        try {
            encoded = EncodedImage.create("/load.png");
            // Pro only feature, uncomment if you have a pro subscription
            // Log.bindCrashProtection(true);
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        UIBuilder ui = new UIBuilder();
        f = new Form("Nos familles", BoxLayout.y());
        f.getToolbar().addCommandToLeftBar("BAck",null,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
            ProfileForm h = new ProfileForm(theme, "home");
            h.getF().show();
            }
        });

        ArrayList<Integer> li = getListFamilles();
        ArrayList<String> ls = getListNomFamilles(li);
        ArrayList<String> listimage = getListImageFamilles(li);
       


        for (int i = 0; i < ls.size(); i++) {
            System.out.println("zgzegv"+ls.get(i)+"");
            
            Label l = new Label(ls.get(i).toString());
            f.add(l);
            Image imgServer = URLImage.createToStorage(encoded, listimage.get(i), "http://localhost/pi/" + listimage.get(i));
            System.out.println("http://localhost/pi/" + listimage.get(i));
            ImageViewer imgProfile = new ImageViewer();
            imgProfile.setImage(imgServer);
            f.add(imgProfile);
         
            
        }
        
           f.getToolbar().addCommandToSideMenu("Profile", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           ProfileForm about = new ProfileForm(theme,"s");
           about.getF().show();
            }
        });
        f.getToolbar().addCommandToSideMenu("Sortie en Famille", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           SortirEnFamilleForm about = new SortirEnFamilleForm(theme);
           about.getF().show();
            }
        });
        
          f.getToolbar().addCommandToSideMenu("Bons plans", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                BonPlanForm bf =new BonPlanForm();
                bf.afficher(theme);
            }
        });
     
        
        f.getToolbar().addCommandToSideMenu("Nos évènements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           PlanAjout about = new PlanAjout();
           about.getF().show();
            }
        });
             f.getToolbar().addCommandToSideMenu("Mes reservations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           mesreservationForm about = new mesreservationForm();
           about.afficherres(theme);
            }
        });
               f.getToolbar().addCommandToSideMenu("Gérer Evenements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              PlanAjout ajout =  new PlanAjout();
             
                       

                       
                    }
        });
        
          f.getToolbar().addCommandToSideMenu("Faire une réclamation", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AjoutForm ajt = new AjoutForm(theme);
           ajt.getF().show();
            }
        });
        f.getToolbar().addCommandToSideMenu("Voir les réclamations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AfficherForm aff = new AfficherForm(theme);
           aff.getF().show();
            }
        });
         f.getToolbar().addCommandToSideMenu("ajouter un commentaire", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           ajoutCom myCom = new ajoutCom(theme);
           myCom.getF().show();
            }
        });
          f.getToolbar().addCommandToSideMenu("Voir les commentaires", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           afficheCom myCom = new  afficheCom(theme);
           myCom.getF().show();
            }
        });
        

    }

    public void setF(Form f) {
        this.f = f;
    }

    public Form getF() {
        return f;
    }

    public ArrayList<String> getListImageFamilles(ArrayList<Integer> listIds) {
        ArrayList<String> listimage = new ArrayList<>();

        for (Integer x : listIds) {
            ConnectionRequest con2 = new ConnectionRequest();
            con2.setUrl("http://localhost/pi/findbyid.php?id=" + x + "");
            con2.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    listimage.add(getListImageFamillesJson(new String(con2.getResponseData())));
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(con2);

        }

        return listimage;
    }

    public String getListImageFamillesJson(String json) {
        String i = null;
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> familles = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> list = (Map<String, Object>) familles.get("user");
            i = list.get("image").toString();

        } catch (IOException ex) {
        }
        return i;

    }

    public ArrayList<String> getListNomFamilles(ArrayList<Integer> listIds) {
        ArrayList<String> listNoms = new ArrayList<>();
        System.out.println("ddd");

        for (Integer x : listIds) {
            System.out.println("dsss");
            ConnectionRequest con1 = new ConnectionRequest();
            con1.setUrl("http://localhost/pi/findbyid.php?id=" + x + "");
            con1.addResponseListener(new ActionListener<NetworkEvent>() {
                @Override
                public void actionPerformed(NetworkEvent evt) {
                    listNoms.add(getListNomFamillesJson(new String(con1.getResponseData())));
                }
            });
            NetworkManager.getInstance().addToQueueAndWait(con1);

        }

        return listNoms;
    }

    public String getListNomFamillesJson(String json) {
        String s = null;
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> familles = j.parseJSON(new CharArrayReader(json.toCharArray()));
            Map<String, Object> list = (Map<String, Object>) familles.get("user");
            s = list.get("nom").toString();
        } catch (IOException ex) {
        }
        return s;

    }

    public ArrayList<Integer> getListFamilles() {

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectF.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                listIds = getListFamillesJson(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listIds;
    }

    public ArrayList<Integer> getListFamillesJson(String json) {
        ArrayList<Integer> listFamillesId = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> familles = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) familles.get("famille");
            for (Map<String, Object> obj : list) {
                Integer x = Integer.parseInt(obj.get("NomdeFamilleid").toString());
                System.out.println(x);
                listFamillesId.add(x);
            }
        } catch (IOException ex) {
        }
        return listFamillesId;

    }

}
