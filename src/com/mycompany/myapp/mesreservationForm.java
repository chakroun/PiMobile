/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.ParseException;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.models.Reservation;
import com.mycompany.models.User;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hanen
 */
public class mesreservationForm extends Form {
        ArrayList<Reservation> lists = new ArrayList<>();
int u;
int idres;
Form f3= new Form();

    public void afficherres(Resources theme) {
UIBuilder uib = new UIBuilder();
Command cmdBack = new Command("Back");

        f3.getToolbar().addCommandToLeftBar(cmdBack);

        f3.addCommandListener(e -> {

            AccueilForm R = new AccueilForm(theme);
      R.getF().show();

        });
u=LoginForm.getU().getId();
        System.err.println(u);
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/select.php?idu=" +u );
   
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
             public void actionPerformed(NetworkEvent evt) {
                try {
                    ArrayList<Reservation> l = getListser(new String(con.getResponseData()));
                    for (Reservation e : l) {
                        SpanLabel sp = new SpanLabel();
                        Label hanen= new Label();
                        sp.setText(e.toString());
                       //System.out.println(l);
                        Button b = new Button();
                        b.setText("supprimer");
                       hanen.setText(""+e.getId_reservation());
                        f3.add(cellForRow(e,theme));
                         f3.add(b);

                         // idres= e.getId_reservation();       
                     b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              User.setId_reservation(Integer.parseInt(hanen.getText()));
              int hanen2= User.getId_reservation();
                //System.out.println();
                ConnectionRequest req = new ConnectionRequest();
                req.setUrl("http://localhost/pi/removereservation.php?id="+hanen2);
f3.refreshTheme();
              
               

                req.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {

                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                Dialog.show("suppression", "votre reservation a été supprimée aves succes ", "Ok", null);


                            AccueilForm af = new AccueilForm(theme);
                       af.getF().show();
                       
                          //  Dialog.show("Erreur", "erreur", "Ok", null);
                                               
                    }
                });

                NetworkManager.getInstance().addToQueue(req);
               
            }
            
        }); 


                        
                    }
                } catch (ParseException ex) {
                    //Logger.getLogger(mesreservationForm.class.getName()).log(Level.SEVERE, null, ex);
                }
               
            }
        });

        NetworkManager.getInstance().addToQueue(con);
f3.show();
    };
 public ArrayList<Reservation> getListser(String json) throws ParseException {
        JSONParser j = new JSONParser();
        Map<String, Object> services;
        try {
            services = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) services.get("reservation");
SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");


            for (Map<String, Object> obj : list) {
                Reservation s = new Reservation();
                s.setId_reservation(Integer.parseInt(obj.get("id_reservation").toString()));
                s.setNb_placesreserve(Integer.parseInt(obj.get("nb_placesreserve").toString()));
            s.setDate_resrvation( obj.get("date de reservation").toString());
               try{
   int i = Integer.parseInt(obj.get("prix restant").toString());
 s.setPrix_r(Integer.parseInt(obj.get("prix restant").toString()));
}catch(NumberFormatException ex){ 
                     }
          //  s.setDate_resrvation(format.parse(obj.get("date de reservation").toString()));
                lists.add(s);
                
                
            }

        } catch (IOException ex) {
        }
        return lists;

    }
  public Container cellForRow(Reservation s, Resources theme ) {
    
        
        
        
        Container ctn1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container ctn2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Label lblSurnom = new Label("nombre de places reservé : " + s.getNb_placesreserve());

        ctn1.setLeadComponent(lblSurnom);

        ctn2.add(new Label("prix restant à payer  : " + s.getPrix_r()));
         ctn2.add(new Label("date de reservation  : " + s.getDate_resrvation()));

        ctn2.add(lblSurnom);
        lblSurnom.addPointerReleasedListener(e -> {

        });
        ctn1.setLeadComponent(lblSurnom);

        ctn1.add(ctn2);

        return ctn1;
    }
}