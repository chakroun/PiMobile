/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Form;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;

/**
 *
 * @author wiem
 */
public class AboutForm {
       Form f ;
    TextField tfLogin;
 public AboutForm(Resources theme){
     
     UIBuilder ui = new UIBuilder();
     f= ui.createContainer(theme, "About").getComponentForm();
     
    f.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent evt) {
        LoginForm login =  new LoginForm(theme);
        login.getF().showBack();
         }
     });

 }

    public Form getF() {
        return f;
    }
}
