/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;

import java.io.IOException;



public class ProfileForm {

    Form f,f2;
    TextField tfLogin;
    EncodedImage encoded;

    public ProfileForm(Resources theme, String nom) {
        
     
        
        try {
            encoded = EncodedImage.create("/load.png");
            // Pro only feature, uncomment if you have a pro subscription
            // Log.bindCrashProtection(true);
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        f = new Form("Profile", BoxLayout.y());
        Label lem = new Label();
        Label Email = new Label("Email");
        lem.setText(LoginForm.getU().getEmail());
        Label lnom = new Label();
         Label Nomd_famille = new Label("Nom d'utilisateur");
        lnom.setText(LoginForm.getU().getNom());
        Label Nom_Utilisateur = new Label("Username");
        Label lusername = new Label();
         lusername.setText(LoginForm.getU().getUsername());
        
        Image imgServer = URLImage.createToStorage(encoded, LoginForm.getU().getEmail(), "http://localhost/pi/" + LoginForm.getU().getImage());
        System.out.println("http://localhost/pi/" + LoginForm.getU().getImage());
        ImageViewer imgProfile = new ImageViewer();
        imgProfile.setImage(imgServer);
        Button Modif = new Button("Modifier Vos coordonnées");
        Modif.addActionListener(e->{
        ModifUser M = new ModifUser(theme);
        M.getF2().show();
        });
       
      
       ComboBox aj =new ComboBox(LoginForm.getU().getNom());
       Button AjouterVotreFamille = new Button("AjouterVotreFamille");
       AjouterVotreFamille.addActionListener(e->{
         ConnectionRequest req = new ConnectionRequest();
      TextField tfUsername = new TextField(LoginForm.getU().getId());
            req.setUrl("http://localhost/pi/InsertFamille.php?NomdeFamilleid="+LoginForm.getU().getId() );
            req.addResponseListener(new ActionListener<NetworkEvent>() {

                @Override
                public void actionPerformed(NetworkEvent evt) {

                      byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s.length());
                        if (s.charAt(s.length() - 1) == 'k') {
                            Dialog.show("Confirmation", "Ajout ok", "Ok", null);

                        } else {
                            Dialog.show("Erreur", "erreur", "Ok", null);
                        }
                }
            });

            NetworkManager.getInstance().addToQueue(req);
            
             

      
       
       
       
       });
       
//        Email.setUIID("labTitre");
//        Nomd_famille.setUIID("labTitre");
//        Nom_Utilisateur.setUIID("labTitre");
       
       Label Point = new Label("Point");
        Label point = new Label();
         point.setText(String.valueOf(LoginForm.getU().getPoint()));
        f.add(imgProfile);
        f.add(Email);
        f.add(lem);
        f.add(Nom_Utilisateur);
        f.add(lusername);
        f.add(Nomd_famille);
        f.add(lnom);
        f.add(Point);
        f.add(point);
         
        f.add(Modif);
     
        f.add(aj);
        f.add(AjouterVotreFamille);
//        Modif.setUIID("buttonLogin");
        
                
//        Button AjouterFamille = new Button("AjouterFamille");
//        AjouterFamille.addActionListener(e->{
//        System.out.println("hello it's " + LoginForm.getU().getId());
//        ConnectionRequest req = new ConnectionRequest();
//        req.setUrl("http://localhost/codename/pi/insertFamille.php?email=" + User.getEmailuser() + "");
//        
//        
//        });
        
        // f.add(AjouterFamille);
        System.out.println("hello it's " + LoginForm.getU().getId());
        ConnectionRequest req = new ConnectionRequest();
        req.setUrl("http://localhost/pi/select.php?email=" + User.getEmailuser() + "");
           f.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent evt) {
        LoginForm login =  new LoginForm(theme);
        login.getF().showBack();
         }
     });

        
           f.getToolbar().addCommandToSideMenu("NosFamilles", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           NosFamillesForm about = new NosFamillesForm(theme);
           about.getF().show();
            }
        });
                 f.getToolbar().addCommandToSideMenu("Sortie en Famille", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           SortirEnFamilleForm about = new SortirEnFamilleForm(theme);
           about.getF().show();
            }
        });
        
          f.getToolbar().addCommandToSideMenu("Bons plans", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                BonPlanForm bf =new BonPlanForm();
                bf.afficher(theme);
            }
        });
            f.getToolbar().addCommandToSideMenu("Quizz", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                QuizzForm home = new QuizzForm();
                home.afficher(theme);
            }
        });
        f.getToolbar().addCommandToSideMenu("NosFamilles", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           NosFamillesForm about = new NosFamillesForm(theme);
           about.getF().show();
            }
        });
        
        f.getToolbar().addCommandToSideMenu("Nos évènements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           PlanAjout about = new PlanAjout();
           about.getF().show();
            }
        });
             f.getToolbar().addCommandToSideMenu("Mes reservations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           mesreservationForm about = new mesreservationForm();
           about.afficherres(theme);
            }
        });
               f.getToolbar().addCommandToSideMenu("Gérer Evenements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              PlanAjout ajout =  new PlanAjout();
             
                       

                       
                    }
        });
        
          f.getToolbar().addCommandToSideMenu("Faire une réclamation", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AjoutForm ajt = new AjoutForm(theme);
           ajt.getF().show();
            }
        });
        f.getToolbar().addCommandToSideMenu("Voir les réclamations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AfficherForm aff = new AfficherForm(theme);
           aff.getF().show();
            }
        });
         f.getToolbar().addCommandToSideMenu("ajouter un commentaire", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           ajoutCom myCom = new ajoutCom(theme);
           myCom.getF().show();
            }
        });
          f.getToolbar().addCommandToSideMenu("Voir les commentaires", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           afficheCom myCom = new  afficheCom(theme);
           myCom.getF().show();
            }
        });
           
        

    }
    

    public Form getF() {
        return f;
    }

}
