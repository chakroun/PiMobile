/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;

/**
 *
 * 
 */
public class RestaurantForm extends Form {
    Form f ;
    TextField tfLogin;
    Resources theme;
   
    
    public RestaurantForm(Resources theme){

        UIBuilder ui = new UIBuilder();
        f= ui.createContainer(theme, "Restaurant").getComponentForm();

       f.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           LoginForm login =  new LoginForm(theme);
           login.getF().showBack();
            }
        });

    
    f.getToolbar().addCommandToSideMenu("Sortie en Famille", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           SortirEnFamilleForm about = new SortirEnFamilleForm(theme);
           about.getF().show();
            }
        });
        
         f.getToolbar().addCommandToSideMenu("Bons plans", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                BonPlanForm bf =new BonPlanForm();
                bf.afficher(theme);
            }
        });
           f.getToolbar().addCommandToSideMenu("Quizz", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                QuizzForm home = new QuizzForm();
                home.afficher(theme);
            }
        });
        f.getToolbar().addCommandToSideMenu("NosFamilles", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           NosFamillesForm about = new NosFamillesForm(theme);
           about.getF().show();
            }
        });
        
        f.getToolbar().addCommandToSideMenu("Nos évènements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           PlanAjout about = new PlanAjout();
           about.getF().show();
            }
        });
          f.getToolbar().addCommandToSideMenu("Mes reservations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           mesreservationForm about = new mesreservationForm();
           about.afficherres(theme);
            }
        });
  f.getToolbar().addCommandToSideMenu("Gérer Evenements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              PlanAjout ajout =  new PlanAjout();
             
                       

                       
                    }
        });
        
          f.getToolbar().addCommandToSideMenu("Faire une réclamation", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AjoutForm ajt = new AjoutForm(theme);
           ajt.getF().show();
            }
        });
        f.getToolbar().addCommandToSideMenu("Voir les réclamations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AfficherForm aff = new AfficherForm(theme);
           aff.getF().show();
            }
        });
         f.getToolbar().addCommandToSideMenu("ajouter un commentaire", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           ajoutCom myCom = new ajoutCom(theme);
           myCom.getF().show();
            }
        });
          f.getToolbar().addCommandToSideMenu("Voir les commentaires", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           afficheCom myCom = new  afficheCom(theme);
           myCom.getF().show();
            }
        });
           
 }

    public Form getF() {
        return f;
    }
    
    
}
