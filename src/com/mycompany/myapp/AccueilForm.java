/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.components.ImageViewer;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;

/**
 *
 * @author Daddati
 */
public class AccueilForm extends Form {
    Form f;
    public AccueilForm(Resources theme1){
        
        UIBuilder.registerCustomComponent("ImageViewer", ImageViewer.class);
        
        UIBuilder uib = new UIBuilder();
        f= uib.createContainer(theme1,"Accueil").getComponentForm();
       
     

        f.getToolbar().addCommandToSideMenu("Sortie en Famille", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           SortirEnFamilleForm about = new SortirEnFamilleForm(theme1);
           about.getF().show();
            }
        });
        
        f.getToolbar().addCommandToSideMenu("Bon Plan", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           BonPlanForm about = new BonPlanForm();
           about.getF().show();
            }
        });
        f.getToolbar().addCommandToSideMenu("NosFamilles", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           NosFamillesForm about = new NosFamillesForm(theme1);
           about.getF().show();
            }
        });
        
        f.getToolbar().addCommandToSideMenu("Nos évènements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           PlanAjout about = new PlanAjout();
           about.getF().show();
            }
        });
             f.getToolbar().addCommandToSideMenu("Mes reservations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           mesreservationForm about = new mesreservationForm();
           about.afficherres(theme1);
            }
        });
               f.getToolbar().addCommandToSideMenu("Gérer Evenements", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              PlanAjout ajout =  new PlanAjout();
             
                       

                       
                    }
        });
        
          f.getToolbar().addCommandToSideMenu("Faire une réclamation", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AjoutForm ajt = new AjoutForm(theme1);
           ajt.getF().show();
            }
        });
          f.getToolbar().addCommandToSideMenu("Quizz", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                QuizzForm home = new QuizzForm();
                home.afficher(theme1);
            }
        });
        f.getToolbar().addCommandToSideMenu("Voir les réclamations", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           AfficherForm aff = new AfficherForm(theme1);
           aff.getF().show();
            }
        });
         f.getToolbar().addCommandToSideMenu("ajouter un commentaire", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           ajoutCom myCom = new ajoutCom(theme1);
           myCom.getF().show();
            }
        });
          f.getToolbar().addCommandToSideMenu("Voir les commentaires", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
           afficheCom myCom = new  afficheCom(theme1);
           myCom.getF().show();
            }
        });
           
        
    }

   

    public Form getF() {
        return f;
    }
    
    
    
}
