/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.mycompany.models.Promotion;
import com.codename1.components.ImageViewer;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.Calendar;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.DateSpinner;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.myapp.AboutForm;
import com.mycompany.myapp.CinemaForm;
import com.mycompany.myapp.HomeForm;
import com.mycompany.myapp.LoginForm;
import com.mycompany.myapp.NosFamillesForm;
import com.mycompany.models.Service;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.FacebookType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hanen
 */
public class BonPlanForm extends Form {

    private Form current, listt, menu, f, f1;
    public Resources theme;
    EncodedImage encoded;
    public int id_s;
    public double p;
    public int nbplaces, nbplacesD;
    public int restant;
    public int houssem;
    int id = 0;

    ArrayList<Promotion> lists = new ArrayList<>();

    public void afficher(Resources theme) {
try {
            encoded = EncodedImage.create("/load.png");
            // Pro only feature, uncomment if you have a pro subscription
            // Log.bindCrashProtection(true);
        } catch (IOException ex) {
            //Logger.getLogger(MyApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        UIBuilder uib = new UIBuilder();
        Container ctn2 = new Container();
        f1 = (Form) uib.findByName("accueil", ctn2);
        Command cmd1 = new Command("promotions");
        SpanLabel sp = new SpanLabel();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectPromo.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListser(new String(con.getResponseData())));
                ArrayList<Promotion> l = getListser(new String(con.getResponseData()));
                for (Promotion e : l) {
                    SpanLabel sp = new SpanLabel();
//                  f1.add(sp);
                    sp.setText(e.toString());
                    Button b = new Button();
                    // f1.add(b);
                    b.setText("reserver");
                }
            }
        });

        NetworkManager.getInstance().addToQueue(con);

    }

    public ArrayList<Promotion> getListser(String json) {
        JSONParser j = new JSONParser();
        Map<String, Object> services;
        try {
            services = j.parseJSON(new CharArrayReader(json.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) services.get("promotions");
            listt = new Form("List", BoxLayout.y());

            for (Map<String, Object> obj : list) {
                Promotion s = new Promotion();
                Service ser = new Service();
                s.setDescription(obj.get("description").toString());
                s.setPrix_promo(Float.valueOf(obj.get("prix_promo").toString()));
                s.setService(new Service(Integer.parseInt(obj.get("id_service").toString())));
                ser.setCategorie(obj.get("categorie").toString());
                ser.setPrix(Float.valueOf(obj.get("prix").toString()));
                ser.setImageService(obj.get("image_service").toString());
                lists.add(s);

                listt.add(cellForRow(s,ser, theme));
                listt.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        LoginForm login = new LoginForm(theme);
                        login.getF().showBack();
                    }
                });
                
               
                listt.getToolbar().addCommandToSideMenu("Sortir en Famille", null, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        AboutForm about = new AboutForm(theme);
                        about.getF().show();
                    }
                });
                listt.getToolbar().addCommandToSideMenu("Profile", null, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        HomeForm h = new HomeForm(theme, null);
                        h.getF().show();
                    }
                });
                listt.getToolbar().addCommandToSideMenu("Bons plans", null, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        BonPlanForm bf = new BonPlanForm();
                        bf.afficher(theme);
                    }
                });

                listt.getToolbar().addCommandToSideMenu("Quizz", null, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        QuizzForm home = new QuizzForm();
                        home.afficher(theme);
                    }
                });
                listt.getToolbar().addCommandToSideMenu("Nos familles", null, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        NosFamillesForm f = new NosFamillesForm(theme);
                        f.getF().show();

                    }
                });
                listt.show();
            }
        } catch (IOException ex) {
        }
        return lists;

    }

    private Container cellForRow(Promotion s,Service ser, Resources theme) {

        Container ctn1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container ctn2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));

        Label lblSurnom = new Label("libelle : " + s.getDescription());
        Label lb = new Label(" ");

        ctn1.setLeadComponent(lblSurnom);
        
        ctn2.add(lblSurnom);
        ctn2.add(new Label("prix : " + ser.getPrix()));
        ctn2.add(new Label("prix promotion : " + s.getPrix_promo()));
        Image imgServer = URLImage.createToStorage(encoded, ser.getImageService(), "http://localhost/sprintmobile/pi/" + ser.getImageService());
        System.out.println("http://localhost/sprintmobile/pi/" + ser.getImageService());
        ImageViewer imgProfile = new ImageViewer();
        imgProfile.setImage(imgServer);
        ctn2.add(lb);
        ctn2.add(imgProfile);

        lblSurnom.addPointerReleasedListener(e -> {

            //display detail
            displayres(s,ser, theme);
            id_s = s.getIdPromo();
            System.err.println(id_s);
            p = s.getPrix_promo();
            System.out.println(p);
            nbplacesD = s.getIdPromo();
            System.out.println(restant + "rrr");
        });

        ctn1.setLeadComponent(lblSurnom);

        ctn1.add(ctn2);
        id = s.getService().getIdService();
        return ctn1;
    }

    public void recuperer(Resources theme) {

        UIBuilder uib = new UIBuilder();
        Container ctn2 = new Container();
        f1 = (Form) uib.findByName("accueil", ctn2);
        Command cmd1 = new Command("services");
        SpanLabel sp = new SpanLabel();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/sprintmobile/pi/selectService.php?idS=id");
        con.addResponseListener(new ActionListener<NetworkEvent>() {

            @Override
            public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListser(new String(con.getResponseData())));
                ArrayList<String> l = new ArrayList<>();
                l.add(new String(con.getResponseData()));
                Service s = new Service();
                for (String e : l) {
                    SpanLabel sp = new SpanLabel();
//                  f1.add(sp);
                    sp.setText(e.toString());
                    Button b = new Button();
                    // f1.add(b);
                    b.setText("reserver");
                }
            }
        });

        NetworkManager.getInstance().addToQueue(con);

    }

    public void displayres(Promotion s, Service ser ,Resources theme) {
        Command cmd1 = new Command("accueil");

       // UIBuilder.registerCustomComponent("DateSpinner", DateSpinner.class);
        UIBuilder uib = new UIBuilder();

        //Container ctn1 = uib.createContainer(theme, "reservation");
        Form f = new Form("Promotion", BoxLayout.y());

        Label l1 = new Label();
        l1.setText(s.getDescription());
        Label l2 = new Label();
        l2.setText(String.valueOf(s.getPrix_promo()));
        Label l3 = new Label();
        l3.setText(String.valueOf(ser.getCategorie()));
        Label l4 = new Label();
        l3.setText(String.valueOf(ser.getPrix()));

//          String datef = date.getFormatter().format("yyyy-MM-dd");
        Button Log = new Button("Partager");
        Log.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                String accessToken = "EAACEdEose0cBAEZA6soBdbFzVKUYCEQHjPVNtRg1pQJ4ZAGMeqNdlZAd4bQ2PsQGT01VAmHUmmGfsubvEbei0IjG5zGX0hKrVmjtUqrMPtZAL7LsIg5tUQMZCzhyDKZCbfv3h0Rk8FOexlsKDKz2MotGRtJkc48xZAP1kRO6BU74sV8KnHqTh7JoqgpfZC4btJ0ZD";

                FacebookClient fbClient = new DefaultFacebookClient(accessToken);
                FacebookType response = fbClient.publish("me/feed", FacebookType.class,
                        Parameter.with("message", "offre special : " + s.getDescription() + ", Visitez notre site pour plus d'informations "),
                        Parameter.with("link", "https://127.168.0.1/PIdev/web/app_dev.php/responsable/offre/"));
                System.out.println("Votre offre à été publiée");
                System.out.println("fb.com/" + response.getId());
            }
        });
        f.add(l1);
        f.add(l3);
        f.add(l4);
        f.add(l2);

        Log.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                recuperer(theme);
            }
        });
        f.add(Log);

        f.show();

        Command cmdBack = new Command("Back");

        f.getToolbar().addCommandToLeftBar(cmdBack);

        f.addCommandListener(e -> {

            listt.showBack();

        });

        f.show();

    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public void show() {

        f.show();
    }

    public void stop() {
        current = Display.getInstance().getCurrent();
        if (current instanceof Dialog) {
            ((Dialog) current).dispose();
            current = Display.getInstance().getCurrent();
        }
    }

}
