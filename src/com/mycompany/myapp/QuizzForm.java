/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.mycompany.models.Quizz;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Command;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.myapp.AboutForm;
import com.mycompany.myapp.HomeForm;
import com.mycompany.myapp.LoginForm;
import com.mycompany.myapp.NosFamillesForm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Daddati
 */
public class QuizzForm {
                int repSuccniv1 = 0 ;
             int repEchniv1 = 0;
    private Form current,listt,menu,f,f1;
    public Resources theme;
    EncodedImage encoded;
          Form form = new Form();
public int id_s;
public double p;
public int  nbplaces,nbplacesD;
public int restant;
public int houssem;
int i = 0;
int ii = 1;
        ArrayList<Quizz> lists = new ArrayList<>();
       ArrayList<String> response = new ArrayList<String>();
       ArrayList<String> responseC = new ArrayList<String>();
                     Button b = new Button("Valider");

            public void afficher(Resources theme) {

  UIBuilder uib= new UIBuilder();
        Container ctn2= new Container();
       f1=(Form)uib.findByName("accueil", ctn2);
Command cmd1 = new  Command ("quizzs");
       SpanLabel sp = new SpanLabel();
       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectQuizz.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
         
            @Override
            public void actionPerformed(NetworkEvent evt) {
               //System.out.println(getListser(new String(con.getResponseData())));
                  ArrayList<Quizz> l = getListser(new String(con.getResponseData()));
                  for (Quizz e : l) {
                  SpanLabel sp = new SpanLabel();
//                  f1.add(sp);
                  sp.setText(e.toString());
                  Button b = new Button();
                 // f1.add(b);
                  b.setText("reserver");
            }}
        });
        
        NetworkManager.getInstance().addToQueue(con);
       
    }
       
    public ArrayList<Quizz> getListser(String json) {
            JSONParser j = new JSONParser();
           Map<String, Object> services;
        try {
            services = j.parseJSON(new CharArrayReader(json.toCharArray()));
        

           List<Map<String, Object>> list = (List<Map<String, Object>>) services.get("quizzs");
listt = new Form("List", BoxLayout.y());


 for (Map<String, Object> obj : list) {
     i++;
              Quizz s = new Quizz();
            s.setQuestion(obj.get("question").toString());
            s.setReponse1((obj.get("reponse1").toString()));
            s.setReponse2((obj.get("reponse2").toString()));
            s.setReponse3((obj.get("reponse3").toString()));
            s.setReponse_correcte((obj.get("reponse_correcte").toString()));
            responseC.add(s.getReponse_correcte());

            Container c1 = new Container();
            Container c2 = new Container();
            Container c3 = new Container();
            Container ctn3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
            Container ctn7 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
       
        Label lb1 = new Label("         ");
        Label lb2 = new Label("         ");
        Label lb3 = new Label("         ");
        Label lb = new Label(" ");
        Label lb4 = new Label(" ");
        Label lblSurnom = new Label("Question "+i+" : " + s.getQuestion());
        Label r1 = new Label("Reponse 1 : " + s.getReponse1());
        Label r2 = new Label("Reponse 2 : " + s.getReponse2());
        Label r3 = new Label("Reponse 3 : " + s.getReponse3());
        RadioButton a = new RadioButton();
        RadioButton aa = new RadioButton();
        RadioButton aaa = new RadioButton();
        ButtonGroup bg = new ButtonGroup(a,aa,aaa);
        int test = bg.getSelectedIndex();
        System.out.println(test);
 c1.add(lb1);
 c1.add(a);
 c1.add(r1);

 c2.add(lb2);
 c2.add(aa);
 c2.add(r2);
 
 c3.add(lb3);
 c3.add(aaa);
 c3.add(r3);
ctn3.add(c1);
ctn3.add(c2);
ctn3.add(c3);
ctn7.add(lblSurnom);
ctn7.add(lb);
ctn7.add(ctn3);
ctn7.add(lb4);

form.add(ctn7);
// form.add(cellForRow(s,theme));
   
 b.addActionListener(new ActionListener(){
       public void actionPerformed(ActionEvent evt) {
        int test = bg.getSelectedIndex();


         if(a.isSelected())
            {
                response.add(s.getReponse1());
            }
            else if(aa.isSelected())
            {
                response.add(s.getReponse2());
            }
             else if(aaa.isSelected())
            {
                response.add(s.getReponse3());
            }
             if (ii==list.size()) {
                 checkRep(response, responseC);
           }
           ii++;
       
//ResultatForm about = new ResultatForm(theme);
//           about.getRes().show();
        
       }
 });
            }

 form.add(b);
  form.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent evt) {
        LoginForm login =  new LoginForm(theme);
        login.getF().showBack();
         }
     });

        form.getToolbar().addCommandToSideMenu("Sortir en Famille", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                AboutForm about = new AboutForm(theme);
                about.getF().show();
            }
        });
       
        form.getToolbar().addCommandToSideMenu("Profile", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfileForm about = new ProfileForm(theme, null);
                about.getF().show();

            }
        });
           form.getToolbar().addCommandToSideMenu("Bons plans", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                BonPlanForm bf =new BonPlanForm();
                bf.afficher(theme);
            }
        });
        
        form.getToolbar().addCommandToSideMenu("Quizz", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                QuizzForm home = new QuizzForm();
                home.afficher(theme);
            }
        });
        form.getToolbar().addCommandToSideMenu("Nos familles", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                NosFamillesForm f = new NosFamillesForm(theme);
                f.getF().show();

            }
        });
         form.show();
 } catch (IOException ex) {
        }
             return lists;

    }
    

 private Container cellForRow(Quizz s,Resources theme) {

        Container ctn1 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container ctn2 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container ctn3 = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Container ctn4 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container ctn5 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Container ctn6 = new Container(new BoxLayout(BoxLayout.X_AXIS));
        Label lblSurnom = new Label("Question "+i+" : " + s.getQuestion());
        Label r1 = new Label("Reponse 1 : " + s.getReponse1());
        Label r2 = new Label("Reponse 2 : " + s.getReponse2());
        Label r3 = new Label("Reponse 3 : " + s.getReponse3());
        Label lb = new Label(" ");
        Label lb4 = new Label(" ");
        Label lb1 = new Label("         ");
        Label lb2 = new Label("         ");
        Label lb3 = new Label("         ");
        RadioButton rb1 = new RadioButton();
        RadioButton rb2 = new RadioButton();
        RadioButton rb3 = new RadioButton();
        rb3.setSelected(true);
        ButtonGroup bg = new ButtonGroup(rb1,rb2,rb3);
  
        int test = bg.getSelectedIndex();
        System.out.println(test);
       
        ctn1.setLeadComponent(lblSurnom);
        ctn2.add(lblSurnom);
        ctn2.add(lb);
        ctn4.add(rb1);
        ctn4.add(lb1);
        ctn4.add(r1);
        
        ctn5.add(rb2);
        ctn5.add(lb2);
        ctn5.add(r2);
        
        ctn6.add(rb3);
        ctn6.add(lb3);
        ctn6.add(r3);
        
        ctn3.add(ctn4);
        ctn3.add(ctn5);
        ctn3.add(ctn6);
        ctn2.add(ctn3);
        ctn2.add(lb4);
        
        ctn1.setLeadComponent(lblSurnom);

        ctn1.add(ctn2);

        return ctn1;
    }
  public void displayres(Quizz s,Resources theme)
    {
        Command cmd1 = new  Command ("accueil");

       // UIBuilder.registerCustomComponent("DateSpinner", DateSpinner.class);

        UIBuilder uib = new UIBuilder();
        
          //Container ctn1 = uib.createContainer(theme, "reservation");
          Form f = new Form("reservation", BoxLayout.y());



       TextField nbplace = new TextField( );
       SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Picker date = new Picker();
//          String datef = date.getFormatter().format("yyyy-MM-dd");
Button Log = new Button("Insert");
          f.add(nbplace);
         f.add(date);
           f.add(Log);
        
        
         Log.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              nbplaces =Integer.parseInt(nbplace.getText()) ;
              restant =  nbplacesD- nbplaces;
              
              

                ConnectionRequest req = new ConnectionRequest();
                req.setUrl("http://localhost/pi/insert.php?nb_placesreserve=" + nbplaces + "&date_reservation=" + date.getText() +"&id=" +id_s +"&prix=" +p +"&placesrestant=" +restant);

s.setIdQuizz(restant);        
                System.out.println("http://localhost/pi/insert.php?nb_placesreserve=" + nbplace.getText() + "&date_reservation=" + date.getText() +"&id=" +id_s+  "");
  
 
            System.out.println(restant+"fdddddd");
                req.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                        if (s.equals("success")) {
                            Dialog.show("Confirmation", "reservation effectué avec succes", "Ok", null);
                        } else {
                           // Dialog.show("Erreur", "erreur", "Ok", null);
                        }
                    }
                });

                NetworkManager.getInstance().addToQueue(req);
            }
        });

        f.show();
        
        Command cmdBack = new Command("Back");
        
        f.getToolbar().addCommandToLeftBar(cmdBack);
        
        f.addCommandListener(e->{
        
            listt.showBack();
        
        });                 

f.show();

}
 public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    
    
 public void show(){
     
      f.show();
 }
    
    public void stop() {
        current = Display.getInstance().getCurrent();
        if(current instanceof Dialog) {
            ((Dialog)current).dispose();
            current = Display.getInstance().getCurrent();
        }
    }      
     public void checkRep(ArrayList<String>x,ArrayList<String>y)
    {
                  Iterator<String> iter2 = x.iterator();
                 Iterator<String> iter1 = y.iterator();
       while (iter1.hasNext()&& iter2.hasNext()) {
            if(iter1.next().toString().equals(iter2.next().toString())){
                repSuccniv1++;
            }
        }
                 repEchniv1 = i - repSuccniv1;
        System.out.println("reponses correctes = "+repSuccniv1);
        System.out.println("reponses fausses = "+repEchniv1);
        Form form1 = new Form();
        Label lab1 = new Label();
        Label lab2 = new Label();
        Label lab3 = new Label();

        lab1.setText("Reponses correctes  "+ repSuccniv1);
        lab2.setText("Reponses fausses  "+ repEchniv1);
        
        lab3.setText("Vouz avez gagner "+ repSuccniv1*10 + "points");
      
        int p = ((LoginForm.getU().getPoint())+(repSuccniv1*10));
         ConnectionRequest reqqq = new ConnectionRequest();
            reqqq.setUrl("http://localhost/pi/point.php?id=" + LoginForm.getU().getId() + "&p=" + p + "");
             NetworkManager.getInstance().addToQueue(reqqq);
               LoginForm.getU().setPoint(LoginForm.getU().getPoint()+(repSuccniv1*10));
        form1.add(lab1);
        form1.add(lab2);
        form1.add(lab3);
        
         form1.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent evt) {
        LoginForm login =  new LoginForm(theme);
        login.getF().showBack();
         }
     });

        form1.getToolbar().addCommandToSideMenu("Sortir en Famille", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                AboutForm about = new AboutForm(theme);
                about.getF().show();
            }
        });
       
        form1.getToolbar().addCommandToSideMenu("Profile", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                ProfileForm about = new ProfileForm(theme, null);
                about.getF().show();

            }
        });
         form1.getToolbar().addCommandToSideMenu("Bons plans", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                BonPlanForm bf =new BonPlanForm();
                bf.afficher(theme);
            }
        });
        
        form1.getToolbar().addCommandToSideMenu("Quizz", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                QuizzForm home = new QuizzForm();
                home.afficher(theme);
            }
        });
        form1.getToolbar().addCommandToSideMenu("Nos familles", null, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                NosFamillesForm f = new NosFamillesForm(theme);
                f.getF().show();

            }
        });
        form1.show();
    }    
}
