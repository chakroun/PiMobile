/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.mycompany.models.Reclamation;
import com.codename1.components.SpanLabel;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import static com.mycompany.myapp.afficheCom.myTheme;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author DIARRA
 */
public class AfficherForm {
   public Form f;
private Button afficher;
     private Button ajouter,supprimer,getRId,modifier;
     public static Resources myTheme;
     public TextField search;
     public Button rechercher;
     
    public AfficherForm(Resources theme) {
         UIBuilder ui = new UIBuilder();
         myTheme=theme;
     f= ui.createContainer(theme, "Afficher").getComponentForm();
     f.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent evt) {
        LoginForm login =  new LoginForm(theme);
        login.getF().showBack();
         }
     });
    
       search=new TextField();
     f.add(search);
      rechercher=new Button("Rechercher");
        f.add(rechercher);
        
        rechercher.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent evt) {
                 ConnectionRequest con1 = new ConnectionRequest();
        con1.setUrl("http://localhost/pi/find.php?nom="+search.getText()); 
         con1.addResponseListener(new ActionListener<NetworkEvent>() {
            
             @Override
            public void actionPerformed(NetworkEvent evt) {
                Form hi = new Form("La liste de tout ");
        SpanLabel sp = new SpanLabel();
        hi.add(sp);
                System.out.println(getListReclamation(new String(con1.getResponseData())));
                for(Reclamation i:getListReclamation(new String(con1.getResponseData())))
                {
                    hi.addComponent(reclamation(i));
                }
                //sp.setText(getListReclamationew String(con.getResponseData())) + "");
                hi.refreshTheme();
    hi.show();
            }
            
        });
        //Dialog.show("bien joué", "ok vs etes pret", "ok",null);
         NetworkManager.getInstance().addToQueue(con1);
             }
         });
     
      // Form hi = new Form("La liste de tout ");
        SpanLabel sp = new SpanLabel();
        f.add(sp);
        
     

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectReclamation.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            
             @Override
            public void actionPerformed(NetworkEvent evt) {
               System.out.println(getListReclamation(new String(con.getResponseData())));
                for(Reclamation i:getListReclamation(new String(con.getResponseData())))
                {
                    f.addComponent(reclamation(i));
                }
                //sp.setText(getListReclamationew String(con.getResponseData())) + "");
                f.refreshTheme();

            }
        });
        NetworkManager.getInstance().addToQueue(con);
        
        f.show();
}
    
     public ArrayList<Reclamation> getListReclamation(String json) {
        ArrayList<Reclamation> listReclamations = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> reclamations = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) reclamations.get("reclamation");

            for (Map<String, Object> obj : list) {
               Reclamation e = new Reclamation();
                 e.setId(Integer.parseInt(obj.get("id").toString()));
                 e.setNom(obj.get("nom").toString());
                 e.setSujet(obj.get("sujet").toString());
                 e.setEtat(obj.get("etat").toString());
                 e.setNature(obj.get("nature").toString());
                 e.setLevel(Integer.parseInt(obj.get("level").toString()));
                 e.setEmail(obj.get("email").toString());
                 e.setDate(obj.get("date").toString());
                 e.setDescription(obj.get("description").toString());
               
                
               listReclamations.add(e);

            }

        } catch (IOException ex) {
         }
        return listReclamations;

    }
     
     public ArrayList<Reclamation> find(String json) {
        ArrayList<Reclamation> listReclamations = new ArrayList<>();
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> reclamations = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) reclamations.get("reclamation");

            for (Map<String, Object> obj : list) {
               Reclamation e = new Reclamation();
                 e.setId(Integer.parseInt(obj.get("id").toString()));
                e.setSujet(obj.get("sujet").toString());
                e.setNature(obj.get("nature").toString());
                e.setDate(obj.get("date").toString());
                e.setDescription(obj.get("description").toString());
                e.setNom(obj.get("nom").toString());
                e.setEtat(obj.get("etat").toString());
                e.setLevel(Integer.parseInt(obj.get("level").toString()));
                e.setEmail(obj.get("email").toString());
              
                
                // e.setEtat(obj.get("etat").toString());
               // e.setLevel(Integer.parseInt(obj.get("telephone").toString()));
               
               
               
                
               listReclamations.add(e);

            }

        } catch (IOException ex) {
         }
        return listReclamations;

    }
      public Container reclamation(Reclamation c){
         Container ct = new Container(new BorderLayout()), ct1 = new Container(new GridLayout(2, 4));
        Label lbl1=new Label("nom");
        Label lbl2=new Label("telephone");
        Label lbl3=new Label("email");
         Label lbl7=new Label("etat");
        int lInt=c.getId();
         Label lbl4=new Label(c.getNom());
        Label lbl5=new Label(String.valueOf(c.getLevel()));
        Label lbl6=new Label(c.getEmail());
         Label lbl8=new Label((c.getEtat()));
        Button b1 = new Button("Voir plus");
         Button b2 = new Button("Supprimer");
        
        // ct1.addComponent(new Label(c.getEmail()));
         ct1.addComponent(lbl1);  ct1.addComponent(lbl2);ct1.addComponent(lbl3);ct1.addComponent(lbl7);
          ct1.addComponent(lbl4); ct1.addComponent(lbl5);ct1.addComponent(lbl6);ct1.addComponent(lbl8);
           
         //String a=String.valueOf(lbl8);
        
         b2.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent evt) {
                 String bb=c.infoId();
                 int bbb=Integer.parseInt(bb);
                 System.err.println("bbb");
                 // Dialog.show("Click", c.infoId(), "Ok",null);
               ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/delete.php?id="+bbb); 
        Dialog.show("Confirmation", "supprimé avec succès", "ok",null);
         NetworkManager.getInstance().addToQueue(con);
         new AfficherForm(myTheme).getF();
             }
         });
           
         Button b = new Button("Voir plus");
         b.setWidth(200);
                 b.addActionListener(new ActionListener() {
             @Override
             public void actionPerformed(ActionEvent evt) {
          Dialog.show("Details", c.AfficherDetails(), "ok",null);
             }
         });
                // ct.setLeadComponent(b);
          ct1.addComponent(b);ct1.addComponent(b2);
         
          ct.add(BorderLayout.CENTER,ct1);
          //ct.addComponent(BorderLayout.EAST,new Label(c.getImage()));
          return ct;
        
     }

    public Form getF() {
        return f;
    }
      
      
}