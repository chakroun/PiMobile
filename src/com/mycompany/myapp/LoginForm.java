/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;


import com.codename1.ext.filechooser.FileChooser;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;

import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.notifications.LocalNotification;
import com.codename1.ui.Button;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.ImageIO;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.codename1.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



public class LoginForm {
public static User u = new User();
String s ;

    public LoginForm() {
    }

    public static User getU() {
        return u;
    }

    public static void setU(User u) {
        LoginForm.u = u;
    }
    Form f, f1;
    TextField tfLogin;
    String reponse, url;
    ConnectionRequest con;
Button btnOk;
   
    String filePath;
    String fileName;
    Image img1, img2, img3;
    public LoginForm(Resources theme)  {
        
        UIBuilder ui = new UIBuilder();
        f = ui.createContainer(theme, "Login").getComponentForm();
     
        
        
     tfLogin = (TextField) ui.findByName("tfLogin", f);
     TextField tfPass = (TextField) ui.findByName("tfPass", f);
     Button btnreg = new Button("register");
        f.add(btnreg);
        btnreg.addActionListener(e -> {
            registerUser();
        });
        Button btnOk = (Button) ui.findByName("btOk", f);

        btnOk.addActionListener(e -> {
            ConnectionRequest req = new ConnectionRequest();
            req.setUrl("http://localhost/codename/pi/login.php?login=" + tfLogin.getText() + "&password=" + tfPass.getText() + "");

            req.addResponseListener(new ActionListener<NetworkEvent>() {

                @Override
                public void actionPerformed(NetworkEvent evt) {

                    byte[] data = (byte[]) evt.getMetaData();

                    String response = new String(req.getResponseData());
                    getListuser(new String(req.getResponseData()), req);
                    // System.out.println("dcssdcc   "+e.getNom());     
                    if (s.equals("sucess")) {
                        Dialog.show("login", "login", "ok", null);

                        ProfileForm home = new ProfileForm(theme, tfLogin.getText());
                        home.getF().show();
                    } else {
                        Dialog.show("Warning", "erreur", "Ok", null);
                    }
                }
            });

            NetworkManager.getInstance().addToQueue(req);
            
             

        });
     
        btnOk.addActionListener(e -> {
            ConnectionRequest req = new ConnectionRequest();
            req.setUrl("http://localhost/pi/login.php?login=" + tfLogin.getText() + "&password=" + tfPass.getText() + "");

            req.addResponseListener(new ActionListener<NetworkEvent>() {

                @Override
                public void actionPerformed(NetworkEvent evt) {

                    byte[] data = (byte[]) evt.getMetaData();
                    String response = new String(req.getResponseData()) ;
                           getListuser(new String(req.getResponseData()), req);
                        // System.out.println("dcssdcc   "+e.getNom());     
                      if (s.equals("sucess")){
//                      Dialog.show("login", "login", "ok",null);
                     
                    ProfileForm ac = new ProfileForm(theme, s);
                            ac.getF().show();
                      }
else {
                        Dialog.show("Warning", "erreur", "Ok", null);
                    }
                }
            });

            NetworkManager.getInstance().addToQueue(req);
        });

    }

    public TextField getTfLogin() {
        return tfLogin;
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }

    public void show() {

        f.show();
    }

    private void registerUser() {
         f1 = new Form("register", BoxLayout.y());
        Label LUsername = new Label("Username");
        TextField tfUsername = new TextField();
        Label LNom = new Label("Nom");
        TextField tfPrenom = new TextField();
        Label LEmail = new Label("Email");
        TextField tfEmail = new TextField();
        Label Lpass = new Label("MotDePasse");
        TextField tfPass = new TextField("", "password");
        Label LPhoto = new Label("Votre_Photo");
        Button b = new Button("Importer votre photo");
       

        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                ActionListener callback = e -> {
                    if (e != null && e.getSource() != null) {
                        filePath = (String) e.getSource();

                        //  Now do something with this file
                    }
                };

                if (FileChooser.isAvailable()) {
                    FileChooser.showOpenDialog(".pdf,application/pdf,.gif,image/gif,.png,image/png,.jpg,image/jpg,.tif,image/tif,.jpeg", callback);
                } else {
                    Display.getInstance().openGallery(callback, Display.GALLERY_IMAGE);
                }

            }
        });
        f1.add(LUsername);
        f1.add(tfUsername);
        f1.add(LNom);
        f1.add(tfPrenom);
        f1.add(LEmail);
        f1.add(tfEmail);
        f1.add(Lpass);
        f1.add(tfPass);
        f1.add(b);
        
        Button btnOk = new Button("register");
        
        f1.add(btnOk);
        
        btnOk.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                int fileNameIndex = filePath.lastIndexOf("/") + 1;
                fileName = filePath.substring(fileNameIndex);
                Image imgg;

                try {
                    imgg = Image.createImage(filePath);
                    ImageIO imgIO = ImageIO.getImageIO();
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    imgIO.save(imgg, out, ImageIO.FORMAT_JPEG, 1);
                    byte[] ba = out.toByteArray();
                    fileName = Base64.encode(ba);

                } catch (IOException ex) {

                }

                ConnectionRequest req2 = new ConnectionRequest();
                req2.setHttpMethod("POST");
                req2.setUrl("http://localhost/pi/insertu.php");

                req2.addArgument("image", fileName);
                req2.addArgument("username", tfUsername.getText());
                req2.addArgument("nom", tfPrenom.getText());
                req2.addArgument("email", tfEmail.getText());
                req2.addArgument("password", tfPass.getText());

                req2.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {

                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s.length());
                        if (s.charAt(s.length() - 1) == 'k') {
                            Dialog.show("Confirmation", "Inscription ok", "Ok", null);

                        } else {
                            Dialog.show("Erreur", "erreur", "Ok", null);
                        }
                    }
                });
//                Message m = new Message("Bonjour :) , bienvenue à Famicity" );
//                 m.sendMessageViaCloudSync("Codename One", "hiba.rajhi.sticc@gmail.com", "Famicity", "Inscription","Check out Codename One at https://www.codenameone.com/&quot");
//                Display.getInstance().sendMessage(new String[] {"hiba.rajhi.sticc@gmail.com"}, "Famicity", m);
                NetworkManager.getInstance().addToQueue(req2);
                LocalNotification n = new LocalNotification();
                System.out.println("Notification envoyée");
                n.setAlertBody("hi");
                n.setAlertTitle("hi there");
                n.setId("1");
                //  n.setBadgeNumber(badgeNumber++);

                Display.getInstance().scheduleLocalNotification(n, System.currentTimeMillis() + 10 * 1000, LocalNotification.REPEAT_NONE);

            }
            
        });

        f1.show();


    }

    public void getListuser(String json, ConnectionRequest con) {
              ArrayList<User> listEtudiants = new ArrayList<>();
        User e = new User();

        try {

            JSONParser j = new JSONParser();

            Map<String, Object> etudiants = j.parseJSON(new CharArrayReader(json.toCharArray()));

            Map<String, Object> ann = (Map<String, Object>) (etudiants.get("user"));

            System.out.println("ann " + ann);

            u.setId(Integer.parseInt(ann.get("id").toString()));
            System.out.println("id " + ann.get("id").toString());
            u.setEmail(ann.get("email").toString());
            u.setImage(ann.get("image").toString());
            u.setNom(ann.get("nom").toString());
            u.setUsername(ann.get("username").toString());
            u.setPassword(ann.get("password").toString());
            u.setPoint(Integer.parseInt(ann.get("point").toString()));
           
            s = ann.get("sucess").toString();
            listEtudiants.add(e);

        } catch (IOException ex) {
        }
        }
    }


