/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.Command;
import com.codename1.ui.Dialog;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author wister
 */
public class ModifUser {

    public Form getF2() {
        return f1;
    }
    Form f1;
    public ModifUser (Resources theme){
      
    
    f1 = new Form("Modifier", BoxLayout.y());
        Label LUsername = new Label("Username");
        TextField tfUsername = new TextField(LoginForm.getU().getUsername());
        Label LNom = new Label("Nom");
        TextField tfPrenom = new TextField(LoginForm.getU().getNom());
         Label LEmail = new Label("Email");
        TextField tfEmail = new TextField(LoginForm.getU().getEmail());
         Label Lpass = new Label("MotDePasse");
        TextField tfPass = new TextField(LoginForm.getU().getPassword());
        Button b = new Button("Modifier");
       
        f1.add(LUsername);
        f1.add(tfUsername);
        f1.add(LNom);
        f1.add(tfPrenom);
        f1.add(LEmail);
        f1.add(tfEmail);
        f1.add(Lpass);
        f1.add(tfPass);
        
       
        f1.add(b);
       
     
         f1.getToolbar().addCommandToOverflowMenu("Logout", null, new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent evt) {
        LoginForm login =  new LoginForm(theme);
        login.getF().showBack();
         }
     });
            f1.getToolbar().addCommandToLeftBar("BAck",null,new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
            ProfileForm h = new ProfileForm(theme, "home");
            h.getF().show();
            }
        });
        b.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                   
                ConnectionRequest req = new ConnectionRequest();           
               req.setUrl("http://localhost/pi/Modifier.php?username=" + tfUsername.getText() + "&nom="+ tfPrenom.getText() +  "&email="+ tfEmail.getText()+  "&password="+ tfPass.getText() +  "&id="+LoginForm.getU().getId()+"");
                
                
                req.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {
                        
                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                        if (s.charAt(s.length()-1)=='k') {
                            Dialog.show("Confirmation", "Modification ok", "Ok", null);
                        } else {
                            Dialog.show("Erreur", "erreur", "Ok", null);
                        }
                    }
                });

                NetworkManager.getInstance().addToQueue(req);
            }
        });
        
        
        
     
        
         
    }
}
