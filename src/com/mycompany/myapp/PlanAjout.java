/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.Button;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Slider;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.spinner.Picker;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UIBuilder;
import com.mycompany.models.Service;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author wiem
 */
public class PlanAjout {

    private Form current, f, hi;
    Container ct = new Container(new BoxLayout(BoxLayout.Y_AXIS));
    private Resources theme;
    public ComboBox<String> sp = new ComboBox<>();
    ArrayList<Service> listService = new ArrayList<>();
    ArrayList<User> listUser = new ArrayList<>();

    ArrayList<Service> listServ = new ArrayList<>();
    List<Service> lise = new ArrayList<>();
    List<User> liseUser = new ArrayList<>();
    public int idServ;
    public int idUser;
    Service s2 = new Service();

   

    public PlanAjout() {
         if (current != null) {
            current.show();
            return;
        }
  

        UIBuilder.registerCustomComponent("Picker", Picker.class);
        UIBuilder uib = new UIBuilder();
  
        Button btnOk = new Button("Ajouter");
        TextField EventName = new TextField();
        Button envoyer=new Button("Envoyer");
        
       
        Picker dateTimePicker = new Picker();
        dateTimePicker.setType(Display.PICKER_TYPE_DATE_AND_TIME);
        hi = new Form("Ajouter un evenement");
        ct.add(EventName);
        Slider sl = new Slider();
        ct.add(dateTimePicker);
        Slider sl0 = new Slider();
        afficherService();
        Slider sl1 = new Slider();
        afficherUser();
        Slider s2 = new Slider();
        ct.add(btnOk);
        ct.add(envoyer);
        btnOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {

                ConnectionRequest req = new ConnectionRequest();
                Service s3 = new Service();
                s3.setIdService(idServ);
                System.out.println(idServ);
                User u3 = new User();
                u3.setId(idUser);
                LoginForm lg = new LoginForm();
                String createur = lg.getU().getNom();
                System.out.println("id_service=" + s3.getIdService());
                req.setUrl("http://localhost/pi/insert.php?id_service=" + s3.getIdService() + "&nom_evenement=" + EventName.getText() + "&horaire_planing=" + dateTimePicker.getDate() + "&NomdeFamilleid=" + u3.getId()+ "&createur=" + createur);

                req.addResponseListener(new ActionListener<NetworkEvent>() {

                    @Override
                    public void actionPerformed(NetworkEvent evt) {

                        byte[] data = (byte[]) evt.getMetaData();
                        String s = new String(data);
                        System.out.println(s);
                        if (s.equals("success")) {
                            Dialog.show("Confirmation", "ajout ok", "Ok", null);
                        } else {
                            Dialog.show("Erreur", "erreur", "Ok", null);
                        }
                    }
                });

                NetworkManager.getInstance().addToQueue(req);
            }
        });
        envoyer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                
                PlanAjout ms = new PlanAjout();
       
                ms.sendMessageCode("24432308",EventName.getText()+": nouvelle evenement est à l'attente de votre confirmation. FAMICITY!");
            }
        });  
        hi.addComponent(ct);
        hi.getToolbar().addCommandToOverflowMenu("Mes Evenements", null, new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent evt) {
          AffichageForm about = new AffichageForm();
         }
     });
        hi.show();
    }
   public String sendMessageCode(String to,String codeTele) {
        
  String myURL="https://rest.nexmo.com/sms/json?api_key=bf0240ee&api_secret=3f75b8a63cf829b9&to=216"+
                to+"&from=56311710&text="+codeTele;
     
     
        System.out.println(myURL);
        StringBuilder sb = new StringBuilder();

        ConnectionRequest con = new ConnectionRequest();
        con.setUrl(myURL); 
        Dialog.show("Message", "Message envoyé", "ok",null);
        NetworkManager.getInstance().addToQueue(con);

        return sb.toString();
    }
    public int afficherService() {
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/pi/selectService.php");
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListEtudiant(new String(con.getResponseData())));
                lise = getListService(new String(con.getResponseData()));
                for (Service p : lise) {
                    sp.addItem(p.getLibelle());
                }
            }
        });
        NetworkManager.getInstance().addToQueue(con);
       ct.add(sp);
        sp.addActionListener(e -> {
            int ind = lise.get(sp.getSelectedIndex()).getIdService();
            System.out.println(ind);
            for (Service p2 : lise) {
                if (p2.getIdService() == ind) {
                    System.out.println("9bal");
                    idServ = ind;
                    System.out.println("s=" + idServ);
                }
            }
        });
        return idServ;
    }

    public int afficherUser() {
        ComboBox<String> sp2 = new ComboBox<>();
        ConnectionRequest con1 = new ConnectionRequest();
        con1.setUrl("http://localhost/pi/selectUserName.php");
        con1.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                //System.out.println(getListEtudiant(new String(con.getResponseData())));
                liseUser = getListUser(new String(con1.getResponseData()));
                for (User p : liseUser) {
                    sp2.addItem(p.getNom());
                }
            }
        });
        NetworkManager.getInstance().addToQueue(con1);
        sp2.addActionListener(e -> {
            int indu = liseUser.get(sp2.getSelectedIndex()).getId();
            System.out.println(indu);
            for (User p3 : liseUser) {
                if (p3.getId() == indu) {
                    idUser = indu;
                }
            }
        });
        ct.add(sp2);
        return idUser;

    }

    public ArrayList<Service> getListService(String json) {
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> service = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) service.get("service");
            for (Map<String, Object> obj : list) {
                Service s = new Service();                
                s.setIdService(Integer.parseInt(obj.get("id_service").toString()));
                s.setLibelle(obj.get("libelle").toString());
                listService.add(s);
            }
        } catch (IOException ex) {
        }

        System.out.println(listService);
        return listService;
    }

    public ArrayList<User> getListUser(String json) {
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> User = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) User.get("fos_user");
            for (Map<String, Object> obj : list) {
                User u = new User();
                u.setId(Integer.parseInt(obj.get("id").toString()));
                u.setNom(obj.get("nom").toString());
                listUser.add(u);
            }
        } catch (IOException ex) {
        }
        System.out.println(listUser);
        return listUser;
    }
      public Form getF() {
        return f;
    }
}
